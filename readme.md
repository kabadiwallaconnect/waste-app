# Waste App

Kabadiwalla Connect has completed development of an open source mobile application to connect kabadiwallas and community members. The application is developed on Ionic, an open-source framework for building hybrid mobile applications. This allows the app to be cross-platform as the same codebase can be deployed on both IOS (iPhone) and Android operating systems. The app will serve two functions: logistical and educational.

The application’s primary feature will be the ability to search and locate a nearby kabadiwalla. This ‘Find a Kabadiwalla’ feature will be based on the user’s GPS coordinates accessed directly from their phone. A list of kabadiwallas within a 2 kilometre radius will be displayed. A user can select one of the displayed kabadiwallas and then she or he has the option of calling the kabadiwalla for a home pick-up or receiving directions to the store to drop off the recyclables. Directions are easily conveyed through a Google Maps integration.

The other feature of the application is a space for community members to learn about recycling through articles on segregation and composting. This is a dynamic list and new articles can be added and old articles removed at will.

Through the application, KC will be also be able to push updates and announcements to users’ phones. This will also help in notifying users of events of interest and anything else that might be of some utility to them.

The code for the application will be open source and therefore freely accessible on the Mediawiki. Other developers will be able to copy, modify and reuse the code to build similar applications that they can deploy in their local contexts.

# Screenshots

![](https://wiki.kabadiwallaconnect.in/images/4/40/App-home.png)
![](https://wiki.kabadiwallaconnect.in/images/e/ea/Composting.png)
