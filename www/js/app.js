// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'ngCordova', 'ngStorage'])

  .run(function($ionicPlatform, $rootScope, $window, $cordovaPushV5, $state) {

    $rootScope.$on('$cordovaPushV5:notificationReceived', function(event, data) {
      $rootScope.$broadcast('messageReceived', {
        message: data.additionalData.data
      });
      console.log("called");
      console.log(data);
      if(!data.additionalData.foreground)
        $state.go('notifications');
    });

      $ionicPlatform.ready(function() {
          if (window.cordova && window.cordova.plugins.Keyboard) {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

            // Don't remove this line unless you know what you are doing. It stops the viewport
            // from snapping when text inputs are focused. Ionic handles this internally for
            // a much nicer keyboard experience.
            cordova.plugins.Keyboard.disableScroll(true);
          }
          if (window.StatusBar) {
            StatusBar.styleDefault();
          }

          $rootScope.notificationEnabled = $window.localStorage["notification_enable"] == 'true' ? true : false;
          if ($window.localStorage['user_disable_token'])
            $rootScope.userDisableNotification = $window.localStorage['user_disable_token'] == 'true' ? true : false;

          if($rootScope.notificationEnabled){
            $cordovaPushV5.initialize({ // important to initialize with the multidevice structure !!
              android: {
                senderID: "763533352809",
                clearNotifications: "false"
              },
              ios: {},
              windows: {}
            });

            $cordovaPushV5.register().then(function(deviceToken) {
              $rootScope.deviceToken = deviceToken;
              $cordovaPushV5.onNotification();
              $cordovaPushV5.onError();
            }, function(error) {
              console.log("Failed to registered");
              console.log("error object : ", error);
            });
          }

        /*$rootScope.$on('$cordovaPushV5:notificationReceived', function(event, data) {
          $rootScope.$broadcast('messageReceived', {
            message: data.additionalData.data
          });
          console.log("called");
          console.log(data);
          if(!data.additionalData.foreground)
            $state.go('notifications');
        });*/

        $rootScope.$on('$cordovaPushV5:errorOccurred', function(event, error) {
          console.log(error);
        });

      });
  })
.factory('AuthService', ['$http', '$window', '$rootScope', '$state', '$ionicHistory',
    function($http, $window, $rootScope, $state, $ionicHistory) {
      var service = {};

      // Authenticates throug a rest service
      service.signup = function(first_name, last_name, email, location) {
        $http({
            url: 'http://gpsdd.kcdev.xyz/api/v1/user/signup',
            method: "POST",
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            },
            transformRequest: function(obj) {
              var str = [];
              for (var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
              return str.join("&");
            },
            data: {
              first_name: first_name,
              last_name: last_name,
              email: email,
              location: location
            }
          })
          .success(function(response) {
            console.log(response);
            $window.localStorage["user"] = JSON.stringify(response);
            $rootScope.user = response;
            $ionicHistory.clearHistory();
            $ionicHistory.nextViewOptions({
              historyRoot: true
            });
            $state.go('home');
          });
      };

      // Creates a cookie and set the Authorization header
      service.setUser = function() {
        $rootScope.user = JSON.parse($window.localStorage["user"]);
      };

      service.authenticated = function() {
        if ($window.localStorage["user"]) {
          this.setUser();
          return true;
        } else {
          return false;
        }
      }

      return service;
    }
  ])
  .config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('signup', {
        url: '/signup',
        templateUrl: 'templates/signup.html',
        controller: 'SignupCtrl'
      }).state('home', {
        url: '/',
        templateUrl: 'templates/home.html',
        controller: 'HomeCtrl',
        resolve: {
          authenticated: function(AuthService, $location) {
            console.log(AuthService.authenticated());
            if (!AuthService.authenticated()) {
              $location.url('/signup');
              return true;
            }
            return false;
          }
        }
      }).state('kabadiwalla', {
        url: '/kabadiwalla',
        templateUrl: 'templates/kabadiwalla.html',
        controller: 'KabadiwallaCtrl'
      }).state('info', {
        url: '/info',
        templateUrl: 'templates/info.html',
        controller: 'InfoCtrl'
      }).state('details', {
        url: '/info/details',
        templateUrl: 'templates/info-details.html',
        controller: 'InfoDetailsCtrl'
      }).state('notifications', {
        url: '/notifications',
        templateUrl: 'templates/notification.html',
        controller: 'NotificationsCtrl'
      }).state('notificationsDetails', {
        url: '/notifications-details',
        templateUrl: 'templates/notification-details.html',
        controller: 'NotificationDetailsCtrl'
      });

    $urlRouterProvider.otherwise("/");

  })
  .controller("HomeCtrl", ['$scope', 'AuthService', '$rootScope', '$state', '$cordovaPushV5', '$window', '$ionicPlatform', '$http', function($scope, AuthService, $rootScope, $state, $cordovaPushV5, $window, $ionicPlatform, $http) {

    $ionicPlatform.ready(function() {

    $rootScope.$on('messageReceived', function(event, data) {
      console.log("received");
    });

      if (!$rootScope.notificationEnabled && !$rootScope.userDisableNotification) {
        $cordovaPushV5.initialize({
          android: {
            senderID: "763533352809",
            clearNotifications: "false"
          },
          ios: {},
          windows: {}
        });

        $cordovaPushV5.register().then(function(deviceToken) {
          console.log(deviceToken);
          console.log($rootScope.user.user_id, $rootScope.user.token);
          $scope.deviceToken = deviceToken;
          $window.localStorage['notification_enable'] = "true";
          $rootScope.notificationEnabled = "true";
          $cordovaPushV5.onNotification();
          $cordovaPushV5.onError();
          $http({
            url: 'http://gpsdd.kcdev.xyz/api/v1/user/fcm/register',
            method: "GET",
            params: {
              push_token: deviceToken,
              user_id: $rootScope.user.user_id,
              token: $rootScope.user.token
            }
          }).success(function(response) {
            console.log(response);
            $window.localStorage['notification_enable'] = "true";
          }).error(function(error) {
            $window.localStorage['notification_enable'] = "false";
          });
        }, function(error) {
          console.log("Failed to registered");
          console.log("error object : ", error);
          $window.localStorage['user_disable_token'] = "true";
        });
      }
    });

  }]).controller("SignupCtrl", ['$scope', 'AuthService', function($scope, auth) {
    $scope.form = {
      location: "Adambakkam"
    };
    $scope.register = function(user) {
      auth.signup(user.first_name, user.last_name, user.email, user.location);
    };

  }]).controller("NotificationsCtrl", ['$scope', '$ionicHistory', '$rootScope', '$http', '$state', function($scope, $ionicHistory, $rootScope, $http, $state) {

    $scope.goBack = function() {
      $ionicHistory.goBack();
    }

    $scope.notifications = [];

    $http({
      method: "GET",
      url: 'http://gpsdd.kcdev.xyz/api/v1/user/notification',
      params: {
        user_id: $rootScope.user.user_id,
        token: $rootScope.user.token
      }
    }).then(function(response){
      console.log(response);
      $scope.notifications = response.data.notifications;
    }, function(error){
      console.log(error);
    });

    $scope.setCurrent = function setCurrent(index) {
      console.log($scope.notifications[index]);
      $rootScope.notification = $scope.notifications[index];
      $state.go("notificationsDetails");
    }

  }]).controller("NotificationDetailsCtrl", ['$scope', '$ionicHistory', '$rootScope', function($scope, $ionicHistory, $rootScope) {

    $scope.notification = $rootScope.notification;

    $scope.goBack = function() {
      $ionicHistory.goBack();
    }



  }]).controller("KabadiwallaCtrl", ['$scope', '$rootScope', '$cordovaGeolocation', "$http", '$ionicPlatform', '$timeout', '$ionicHistory', 'AuthService', function($scope, $rootScope, $cordovaGeolocation, $http, $ionicPlatform, $timeout, $ionicHistory, auth) {

    var posOptions = {
      timeout: 60000,
      enableHighAccuracy: true
    };

    $scope.kabadiwallas = [];
    $scope.loadingMessage = "Fetching your location!"
    $scope.index = 1;
    $scope.lat = null;
    $scope.lng = null;
    $scope.lastIndex = true;
    $scope.loading = true;
    $scope.nokc = false;

    $scope.findKabadiwalla = function() {
      $scope.loadingMessage = "Finding Kabadiwallas near you!"
      $http({
        method: "GET",
        url: "http://gpsdd.kcdev.xyz/api/v1/kw",
        params: {
          user_id: $rootScope.user.user_id,
          token: $rootScope.user.token,
          latitude: $scope.lat,
          longitude: $scope.long,
          index: $scope.index
        }
      }).then(function(response) {
        console.log(response);
        if ($scope.index == 1 && response.data.kw_details.length == 0) {
          console.log("No Kabadiwalla")
          $scope.nokc = true;
          $scope.loadingMessage = "Sorry, couldn't find Kabadiwallas in your location!"
        }
        $scope.kabadiwallas = $scope.kabadiwallas.concat(response.data.kw_details);
        $scope.index = response.data.last_index;
        if (response.data.kw_details.length == 0) {
          $scope.lastIndex = true;
        } else {
          $scope.lastIndex = false;
        }
        $scope.loading = false;
      });
    };

    $scope.getLocation = function() {
      console.log("get location");
      $scope.loading = true;
      $scope.geo = true;
      $cordovaGeolocation
        .getCurrentPosition(posOptions)
        .then(function(position) {
          $scope.lat = position.coords.latitude;
          $scope.long = position.coords.longitude;
          console.log($scope.lat, $scope.long);
          $scope.geo = false;
          $scope.findKabadiwalla();
        }, function(err) {
          console.log(err);
        });
    }

    $ionicPlatform.ready(function() {
      $scope.getLocation();
    });


    $scope.loadMore = function() {
      console.log('loading');
      if ($scope.kabadiwallas.length != 0 && !$scope.lastIndex) {
        $scope.findKabadiwalla();
      }
    };

    $scope.goBack = function() {
      $ionicHistory.goBack();
    }

  }]).controller("InfoCtrl", ['$scope', '$ionicHistory', '$http', '$state', '$rootScope', function($scope, $ionicHistory, $http, $state, $rootScope) {

    $scope.articles = [];

    console.log($scope.currentArticle);

    $scope.setCurrent = function setCurrent(index) {
      console.log($scope.articles[index]);
      $rootScope.article = $scope.articles[index];
      $state.go("details");
    };

    $scope.goBack = function() {
      $ionicHistory.goBack();
    }

    $http({
      method: "GET",
      url: "http://gpsdd.kcdev.xyz/api/v1/user/articles",
      params: {
        user_id: "1",
        token: "c591bf8b0d23364b17b1681c415124"
      }
    }).then(function(response) {
      $scope.articles = response.data.articles;
    });

  }]).controller("InfoDetailsCtrl", ['$scope', '$ionicHistory', '$http', '$state', '$rootScope', function($scope, $ionicHistory, $http, $state, $rootScope) {

    $scope.article = $rootScope.article;

    $scope.goBack = function() {
      $ionicHistory.goBack();
    }

  }]);
